import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import '../auth/Login.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
  // TODO: Add text editing controllers (101)

}

class _HomePageState extends State<HomePage> {
  // TODO: Add text editing controllers (101)
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        title: Text('Home Tutor'),
        backgroundColorStart: Colors.cyan,
        backgroundColorEnd: Colors.indigo,
      ),
      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text('Raja'),
              accountEmail: new Text('testemail@test.com'),
              currentAccountPicture: new CircleAvatar(
                backgroundImage: new NetworkImage('http://i.pravatar.cc/300'),
              ),
            ),
            new ListTile(
              leading: Icon(Icons.person),
              title: new Text('Logout'),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginPage()),
                );
              },
            ),
          ],
        ),
      ),
      body: Center(
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 14.0),
          children: <Widget>[
            SizedBox(height: 10.0),
            Row(children: <Widget>[
              Expanded(child: Divider()),
              Text("HOME PAGE"),
              Expanded(child: Divider()),
            ]),
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginPage()),
                    );
                  },
                  color: Colors.blueGrey,
                  padding: EdgeInsets.all(5.0),
                  child: Column(
                    // Replace with a Row for horizontal icon + text
                    children: <Widget>[Icon(Icons.person_add), Text("Tutors")],
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginPage()),
                    );
                  },
                  color: Colors.grey,
                  padding: EdgeInsets.all(5.0),
                  child: Column(
                    // Replace with a Row for horizontal icon + text
                    children: <Widget>[
                      Icon(Icons.person_add),
                      Text("Learners")
                    ],
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginPage()),
                    );
                  },
                  color: Colors.orange,
                  padding: EdgeInsets.all(5.0),
                  child: Column(
                    // Replace with a Row for horizontal icon + text
                    children: <Widget>[
                      Icon(Icons.person_add),
                      Text("Learners")
                    ],
                  ),
                ),
              ],
            ),
            Row(children: <Widget>[
              Expanded(child: Divider()),
              Text("Summery"),
              Expanded(child: Divider()),
            ]),
           
                 SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,

               child: DataTable(
                  columns: [
                    DataColumn(label: Text("Status")),
                    DataColumn(label: Text("Subject")),
                     DataColumn(label: Text("Tution cost")),
                      DataColumn(label: Text("Subject category")),
                  ],
                rows: [
            DataRow(cells: [
            DataCell(Text("1")),
          DataCell(Text("ZZZZ2")),
          DataCell(Text("ZZZZ2")),
          DataCell(Text("ZZZZ2")),
          ]),

          DataRow(cells: [
            DataCell(Text("1")),
          DataCell(Text("ZZZZ2")),
           DataCell(Text("ZZZZ2")),
          DataCell(Text("ZZZZ2")),
          ]),

          DataRow(cells: [
            DataCell(Text("1")),
          DataCell(Text("ZZZZ2")),
           DataCell(Text("ZZZZ2")),
          DataCell(Text("ZZZZ2")),
          ]),

          DataRow(cells: [DataCell(Text("2")),
          DataCell(Text("asc-cell2")),
           DataCell(Text("ZZZZ2")),
          DataCell(Text("ZZZZ2")),
          ]),],
          //   sortColumnIndex: 0,
          // sortAscending: true,
                ),
                ),
                 ),
              ],
          
        ),
      ),
    );
  }
}

// TODO: Add AccentColorOverride (103)
