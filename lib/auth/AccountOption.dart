import 'package:flutter/material.dart';
import '../DashBoard/Home.dart';
import '../auth/UpdateProfile.dart';
import '../auth/TutorReg.dart';
import '../auth/LearnerReg.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class AccountOption extends StatefulWidget {
  @override
  _AccountOptionState createState() => _AccountOptionState();
  // TODO: Add text editing controllers (101)

}

class _AccountOptionState extends State<AccountOption> {
  // TODO: Add text editing controllers (101)
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: GradientAppBar(
        title: Text('Home Tutor'),
        backgroundColorStart: Colors.cyan,
    backgroundColorEnd: Colors.indigo,
      ),
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          children: <Widget>[
            SizedBox(height: 50.0),
            Column(
              children: <Widget>[
                // Image.asset('assets/index.jpg'),
                SizedBox(height: 12.0),
                Text('Create Account'),
              ],
            ),
            SizedBox(height: 60.0),
            FlatButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => TutorReg()),
                    );
                },
                color: Colors.orange,
                padding: EdgeInsets.all(40.0),
                child: Column( // Replace with a Row for horizontal icon + text
                  children: <Widget>[
                    Icon(Icons.person_add),
                    Text("Become a Tuitor")
                  ],
                ),
              ),
            
         SizedBox(height: 40.0),
            FlatButton(
               onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LearnerReg()),
                    );
                },
                color: Colors.orange,
                padding: EdgeInsets.all(40.0),
                child: Column( // Replace with a Row for horizontal icon + text
                  children: <Widget>[
                    Icon(Icons.person_add),
                    Text("Become a Learner")
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }
}
