import 'package:flutter/material.dart';
import '../DashBoard/Home.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class UpdateProfile extends StatefulWidget {
  @override
  _UpdateProfileState createState() => _UpdateProfileState();
  // TODO: Add text editing controllers (101)

}

class _UpdateProfileState extends State<UpdateProfile> {
  // TODO: Add text editing controllers (101
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        title: Text('Home Tutor'),
        backgroundColorStart: Colors.cyan,
    backgroundColorEnd: Colors.indigo,
      ),
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          children: <Widget>[
            SizedBox(height: 12.0),
            Column(
              children: <Widget>[
                SizedBox(height: 5.0),
                Text('Update Profile'),
              ],
            ),
            SizedBox(height: 40.0),
            TextField(
               style: new TextStyle(
                height: 0.0,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.cyan[900],
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                // border: InputBorder.none,
                filled: false,
                labelText: 'Area of Specialization',
              ),
            ),
             SizedBox(height: 12.0),
            TextField(
               style: new TextStyle(
                height: 0.0,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.cyan[900],
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                // border: InputBorder.none,
                filled: false,
                labelText: 'Academic Level',
              ),
            ),
             SizedBox(height: 12.0),
            TextField(
               style: new TextStyle(
                height: 0.0,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.cyan[900],
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                // border: InputBorder.none,
                filled: false,
                labelText: 'Certification',
              ),
            ),
             SizedBox(height: 12.0),
            TextField(
               style: new TextStyle(
                height: 0.0,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.cyan[900],
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                // border: InputBorder.none,
                filled: false,
                labelText: 'Cert Upload',
              ),
            ),
              SizedBox(height: 12.0),
            TextField(
               style: new TextStyle(
                height: 0.0,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.cyan[900],
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                // border: InputBorder.none,
                filled: false,
                labelText: 'Year of Experience',
              ),
            ),
            SizedBox(height: 12.0),
            TextField(
              style: new TextStyle(
                height: 0.0,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.cyan[900],
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                // border: InputBorder.none,
                filled: false,
                labelText: 'Subject of Specialization',
              ),
            ),
            SizedBox(height: 6.0),
            ButtonBar(
              children: <Widget>[
                RaisedButton(
                  child: Text('Submit Update Profile'),
                  padding: EdgeInsets.all(15.0),
                  color: Colors.cyan[900],
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => HomePage()),
                    );
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
