import 'package:flutter/material.dart';
import '../DashBoard/Home.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class LearnerReg extends StatefulWidget {
  @override
  _LearnerRegState createState() => _LearnerRegState();
  // TODO: Add text editing controllers (101)

}

class _LearnerRegState extends State<LearnerReg> {
  String _value;
  final List<String> _dropdownValues = [
    "One",
    "Two",
    "Three",
    "Four",
    "Five"
  ]; //Th
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: GradientAppBar(
        title: Text('Home Tutor'),
        backgroundColorStart: Colors.cyan,
    backgroundColorEnd: Colors.indigo,
      ),
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          children: <Widget>[
            SizedBox(height: 12.0),
            Column(
              children: <Widget>[
                SizedBox(height: 4.0),
                  Icon(Icons.person_add),
                Text('Register as Learner'),
              ],
            ),
            SizedBox(height: 30.0),
            TextField(
               style: new TextStyle(
                height: 0.0,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.cyan[900],
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                // border: InputBorder.none,
                filled: false,
                labelText: 'Name',
              ),
            ),
             SizedBox(height: 12.0),

            DropdownButton<String>(
              
                        items: [
	
            DropdownMenuItem<String>(
	
              child: Text('Nairobi'),
	
              value: 'Nairobi',
	
            ),
	
            DropdownMenuItem<String>(
	
              child: Text('Kisumu'),
	
              value: 'Kisumu',
	
            ),
	
            DropdownMenuItem<String>(
	
              child: Text('Mombasa'),
	
              value: 'Mombasa',
	
            ),
            DropdownMenuItem<String>(
	
              child: Text('Nakuru'),
	
              value: 'Nakuru',
	
            ),
	
          ],
          onChanged: (String value) {
            setState(() {
              _value = value;
            });
	
          },
           hint: Text('Select Location'),
           value: _value,
             isExpanded: true,
            ),

             SizedBox(height: 12.0),
            TextField(
               style: new TextStyle(
                height: 0.0,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.cyan[900],
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                // border: InputBorder.none,
                filled: false,
                labelText: 'Phone number',
              ),
            ),
            
             SizedBox(height: 24.0),
            TextField(
               style: new TextStyle(
                height: 0.0,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.cyan[900],
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                // border: InputBorder.none,
                filled: false,
                labelText: 'Email',
              ),
            ),
              SizedBox(height: 12.0),
            TextField(
               style: new TextStyle(
                height: 0.0,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.cyan[900],
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                // border: InputBorder.none,
                filled: false,
                labelText: 'Password',
              ),
              obscureText: true,
            ),
            SizedBox(height: 12.0),
            TextField(
              style: new TextStyle(
                height: 0.0,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.cyan[900],
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                // border: InputBorder.none,
                filled: false,
                labelText: 'Confirm password',
              ),
              obscureText: true,
            ),
            SizedBox(height: 6.0),
            ButtonBar(
              children: <Widget>[
                RaisedButton(
                  child: Text('Register Here'),
                  padding: EdgeInsets.all(15.0),
                  color: Colors.cyan[900],
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => HomePage()),
                    );
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
