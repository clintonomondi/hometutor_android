import 'package:flutter/material.dart';
import '../DashBoard/Home.dart';
import '../auth/UpdateProfile.dart';
import '../auth/AccountOption.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';


class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
  // TODO: Add text editing controllers (101)

}

class _LoginPageState extends State<LoginPage> {
  // TODO: Add text editing controllers (101)
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        title: Text('Home Tutor'),
        backgroundColorStart: Colors.cyan,
    backgroundColorEnd: Colors.indigo,
      ),
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          children: <Widget>[
            SizedBox(height: 50.0),
            Column(
              children: <Widget>[
                Image.asset('assets/index.jpg'),
                SizedBox(height: 10.0),
                Text('LOGIN'),
              ],
            ),
            SizedBox(height: 70.0),
            TextField(
              style: new TextStyle(
                height: 0.0,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.cyan[900],
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                // border: InputBorder.none,
                filled: false,
                labelText: 'Phone',
              ),
            ),
            SizedBox(height: 12.0),
            TextField(
              style: new TextStyle(
                height: 0.0,
              ),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.cyan[900],
                  ),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                // border: InputBorder.none,
                filled: false,
                labelText: 'Password',
              ),
              obscureText: true,
            ),
            SizedBox(height: 6.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                InkWell(
                  child: Text("Forget password?"),
                  onTap: () async {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => UpdateProfile()),
                    );
                  },
                ),
                 InkWell(
                  child: Text("Create Account?"),
                  onTap: () async {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AccountOption()),
                    );
                  },
                ),
              ],
            ),
            SizedBox(height: 6.0),
            ButtonBar(
              children: <Widget>[
                RaisedButton(
                  child: Text('Sign In'),
                  padding: EdgeInsets.all(15.0),
                  color: Colors.cyan[900],
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => UpdateProfile()),
                    );
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
